// console.log("Hello World!");


const num = 2;
const cubeNum = num ** 3;
console.log(`The cube of ${num} is ${cubeNum}`);

const address = ["258", "Washington Ave NW", "California", "90011"];
const [streetNo, streetName, state, zipCode] = address;
console.log(`I live at ${streetNo} ${streetName}, ${state} ${zipCode}`);


const animal = {
		name: "Lolong",
		kind: "saltwater crocodile",
		weight: "1075",
		length: "20 ft 3 in"
};
const { name, kind, weight, length } = animal;
console.log(`${name} was a ${kind}. He weighed at ${weight} kgs with a measurement of ${length}.`);

const numbersArr = [ 1, 2, 3, 4, 5];
numbersArr.forEach((number)=> console.log(number));
const [ x1, x2, x3, x4, x5] = numbersArr;
const add = (x1, x2, x3, x4, x5) => x1 + x2 + x3 + x4 + x5;
console.log(add(x1, x2, x3, x4, x5));


class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed
	};
};
const myDog = new Dog("Lesley", 4, "Askal");
console.log(myDog);